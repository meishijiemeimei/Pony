[![Build status](https://img.shields.io/badge/license-BSD-blue.svg)](LICENSE.txt)
[![Build status](https://img.shields.io/badge/haxelib-0.4.0-blue.svg)](http://lib.haxe.org/p/pony)
[![Build status](https://ci.appveyor.com/api/projects/status/83l5njueb4k0ns60?svg=true)](https://ci.appveyor.com/project/AxGord/pony)

<p align="center"><img width="65%" src="http://qlex.ru/pony_logo_hor.svg?v=1"/></p>

Installation
============
Stable version
<pre>haxelib install pony</pre>
Unstable version
<pre>haxelib git pony https://github.com/AxGord/Pony</pre>

Pony good with
==============
* [HUGS (Haxe Unity Glue...Stuff!)](https://github.com/proletariatgames/HUGS)
* [Pixi.js](https://github.com/pixijs/pixi-haxe)
* [Node.js](https://github.com/dionjwa/nodejs-std)
* [Lime or OpenFL](https://github.com/openfl/openfl)
* [Adobe Animate CC (Flash Professional)](http://www.adobe.com/products/animate.html)
* [FlashDevelop Haxe Upgrade Pack](https://github.com/AxGord/FD-Haxe-Up)
* [Base files for PonyNode site engine](https://github.com/AxGord/PonyNode)
* [haxe-continuation](https://github.com/Atry/haxe-continuation)

Sometimes need npm
------------------
* [node-midi](https://github.com/justinlatimer/node-midi)
* [node-mysql](https://github.com/felixge/node-mysql)
* [send](https://github.com/pillarjs/send)
* [NodeJS Library for Facebook](https://github.com/node-facebook/facebook-node-sdk)
* [SPDY Server for node.js](https://github.com/indutny/node-spdy)

Manual
------
- <a href="http://axgord.github.io/Pony/#signals">Signals</a>
- <a href="http://axgord.github.io/Pony/#deltatime">DeltaTime</a>
- <a href="http://axgord.github.io/Pony/#priority">Priority</a>
- <a href="http://axgord.github.io/Pony/#declarator">Declarator</a>

Reference book
--------------
[Explore more feature](http://axgord.github.io/Pony/docs)
